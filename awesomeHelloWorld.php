<?php


/**
 * 驚くべき挨拶の機能
 *
 * @author Masuoka Hideki
 * @version 0.0.1
 */
class awesomeHelloWorld {
	public static $content = '<div style="text-align: center;"><p>Hello World !!</p><p style="font-size: 5em;">AWESOME</p></div>';
	
	public static function sayHello() {
		echo self::$content;
	}
}
